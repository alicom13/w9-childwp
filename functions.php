<?php
// Import style Tema Induk dan Render Anak Tema
add_action( 'wp_enqueue_scripts', 'w9_style' );
function w9_style() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array('parent-style') );
}
// Require / Include File
require_once get_theme_file_path( 'Inc/w9cpt.php' );