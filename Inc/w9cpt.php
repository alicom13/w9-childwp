<?php
/* W9CPT (Custom Post Types)
 * Prefix Post Type wl_9_
 * Prefix Category w9_
 */
/*
 * A. Pengumuman / Informasi
 * B. Video
 * C. Spanduk / Banner
 * */

 if ( ! function_exists( 'wl_9_info' ) ) {
    function wl_9_info() {

        $wl_9_info_slug = get_option( 'wl_9_info_slug', 'info' );

        $labels = array(
            'name'               => esc_html_x( 'Pengumuman', 'General', 'w9-childwp' ),
            'singular_name'      => esc_html_x( 'Pengumuman', 'Singular', 'w9-childwp' ),
            'menu_name'          => esc_html__( 'Pengumuman', 'w9-childwp' ),
            'parent_item_colon'  => esc_html__( 'Pengumuman :', 'w9-childwp' ),
            'all_items'          => esc_html__( 'All Pengumuman', 'w9-childwp' ),
            'view_item'          => esc_html__( 'Lihat Pengumuman', 'w9-childwp' ),
            'add_new_item'       => esc_html__( 'Tambah Pengumuman', 'w9-childwp' ),
            'add_new'            => esc_html__( 'Tambah baru', 'w9-childwp' ),
            'edit_item'          => esc_html__( 'Edit Pengumuman', 'w9-childwp' ),
            'update_item'        => esc_html__( 'Ubah Pengumuman', 'w9-childwp' ),
            'search_items'       => esc_html__( 'Cari Pengumuman', 'w9-childwp' ),
            'not_found'          => esc_html__( 'Tidak Ada', 'w9-childwp' ),
            'not_found_in_trash' => esc_html__( 'Kosong', 'w9-childwp' ),
        );

        $args = array(
            'label'               => esc_html__( 'wl_9_info', 'w9-childwp' ),
            'description'         => esc_html__( 'pengumuman', 'w9-childwp' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'comments', 'post-formats' ),
            'taxonomies'          => array( 'topik' ),
            'hierarchical'        => false,
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 16,
            'menu_icon'           => 'dashicons-megaphone',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'rewrite'             => array( 'slug' => $wl_9_info_slug ),
        );

        register_post_type( 'ws-info', $args );

    }
    add_action( 'init', 'wl_9_info', 0 );
}

if ( ! function_exists( 'w9_topik' ) ) {

    function w9_topik() {
    
        $labels = array(
            'name'                       => esc_html_x( 'Topik', 'General', 'w9-childwp' ),
            'singular_name'              => esc_html_x( 'Topik', 'Singular', 'w9-childwp' ),
            'menu_name'                  => esc_html__( 'Topik', 'w9-childwp' ),
            'all_items'                  => esc_html__( 'All Topik', 'w9-childwp' ),
            'parent_item'                => esc_html__( 'Parent Topik', 'w9-childwp' ),
            'parent_item_colon'          => esc_html__( 'Parent Topik:', 'w9-childwp' ),
            'new_item_name'              => esc_html__( 'Tambah Topik', 'w9-childwp' ),
            'add_new_item'               => esc_html__( 'Tambah Topik baru', 'w9-childwp' ),
            'edit_item'                  => esc_html__( 'Edit Topik', 'w9-childwp' ),
            'update_item'                => esc_html__( 'Ubah Topik', 'w9-childwp' ),
            'separate_items_with_commas' => esc_html__( 'Pisah Topik dengan koma', 'w9-childwp' ),
            'search_items'               => esc_html__( 'Cari Topik', 'w9-childwp' ),
            'add_or_remove_items'        => esc_html__( 'Tambah/Hapus', 'w9-childwp' ),
            'choose_from_most_used'      => esc_html__( 'Pilih Topik populer', 'w9-childwp' ),
            'not_found'                  => esc_html__( 'Tidak ada', 'w9-childwp' ),
        );
    
        $args = array(
            'labels'            => $labels,
            'hierarchical'      => true,
            'public'            => true,
            'show_ui'           => true,
            'show_in_rest'      => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => false,
        );
    
        register_taxonomy( 'ws-topik', array( 'ws-info' ), $args );
    
    }
    add_action( 'init', 'w9_topik', 0 );
}

// Video
if ( ! function_exists( 'wl_9_video' ) ) {
    function wl_9_video() {

        $wl_9_video_slug = get_option( 'wl_9_video_slug', 'video' );

        $labels = array(
            'name'               => esc_html_x( 'Video', 'General', 'w9-childwp' ),
            'singular_name'      => esc_html_x( 'Video', 'Singular', 'w9-childwp' ),
            'menu_name'          => esc_html__( 'Video', 'w9-childwp' ),
            'parent_item_colon'  => esc_html__( 'Video :', 'w9-childwp' ),
            'all_items'          => esc_html__( 'All Video', 'w9-childwp' ),
            'view_item'          => esc_html__( 'Lihat Video', 'w9-childwp' ),
            'add_new_item'       => esc_html__( 'Tambah Video', 'w9-childwp' ),
            'add_new'            => esc_html__( 'Tambah baru', 'w9-childwp' ),
            'edit_item'          => esc_html__( 'Edit Video', 'w9-childwp' ),
            'update_item'        => esc_html__( 'Ubah Video', 'w9-childwp' ),
            'search_items'       => esc_html__( 'Cari Video', 'w9-childwp' ),
            'not_found'          => esc_html__( 'Tidak Ada', 'w9-childwp' ),
            'not_found_in_trash' => esc_html__( 'Kosong', 'w9-childwp' ),
        );

        $args = array(
            'label'               => esc_html__( 'wl_9_video', 'w9-childwp' ),
            'description'         => esc_html__( 'Video', 'w9-childwp' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'comments', 'post-formats' ),
            'taxonomies'          => array( 'Saluran' ),
            'hierarchical'        => false,
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 17,
            'menu_icon'           => 'dashicons-video-alt',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'rewrite'             => array( 'slug' => $wl_9_video_slug ),
        );

        register_post_type( 'ws-video', $args );

    }
    add_action( 'init', 'wl_9_video', 0 );
}

if ( ! function_exists( 'w9_saluran' ) ) {

    function w9_saluran() {
    
        $labels = array(
            'name'                       => esc_html_x( 'Saluran', 'General', 'w9-childwp' ),
            'singular_name'              => esc_html_x( 'Saluran', 'Singular', 'w9-childwp' ),
            'menu_name'                  => esc_html__( 'Saluran', 'w9-childwp' ),
            'all_items'                  => esc_html__( 'All Saluran', 'w9-childwp' ),
            'parent_item'                => esc_html__( 'Parent Saluran', 'w9-childwp' ),
            'parent_item_colon'          => esc_html__( 'Parent Saluran:', 'w9-childwp' ),
            'new_item_name'              => esc_html__( 'Tambah Saluran', 'w9-childwp' ),
            'add_new_item'               => esc_html__( 'Tambah Baru', 'w9-childwp' ),
            'edit_item'                  => esc_html__( 'Edit Saluran', 'w9-childwp' ),
            'update_item'                => esc_html__( 'Ubah Saluran', 'w9-childwp' ),
            'separate_items_with_commas' => esc_html__( 'Pisah Saluran dengan koma', 'w9-childwp' ),
            'search_items'               => esc_html__( 'Cari Saluran', 'w9-childwp' ),
            'add_or_remove_items'        => esc_html__( 'Tambah/Hapus', 'w9-childwp' ),
            'choose_from_most_used'      => esc_html__( 'Pilih Saluran populer', 'w9-childwp' ),
            'not_found'                  => esc_html__( 'Tidak ada', 'w9-childwp' ),
        );
    
        $args = array(
            'labels'            => $labels,
            'hierarchical'      => true,
            'public'            => true,
            'show_ui'           => true,
            'show_in_rest'      => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => false,
        );
    
        register_taxonomy( 'ws-saluran', array( 'ws-video' ), $args );
    
    }
    add_action( 'init', 'w9_saluran', 0 );
}

// Spanduk
// spanduk
if ( ! function_exists( 'wl_9_spanduk' ) ) {
    function wl_9_spanduk() {

        $wl_9_spanduk_slug = get_option( 'wl_9_spanduk_slug', 'spanduk' );

        $labels = array(
            'name'               => esc_html_x( 'Spanduk', 'General', 'w9-childwp' ),
            'singular_name'      => esc_html_x( 'Spanduk', 'Singular', 'w9-childwp' ),
            'menu_name'          => esc_html__( 'Spanduk', 'w9-childwp' ),
            'parent_item_colon'  => esc_html__( 'Spanduk :', 'w9-childwp' ),
            'all_items'          => esc_html__( 'All Spanduk', 'w9-childwp' ),
            'view_item'          => esc_html__( 'Lihat Spanduk', 'w9-childwp' ),
            'add_new_item'       => esc_html__( 'Tambah Spanduk', 'w9-childwp' ),
            'add_new'            => esc_html__( 'Tambah Baru', 'w9-childwp' ),
            'edit_item'          => esc_html__( 'Edit Spanduk', 'w9-childwp' ),
            'update_item'        => esc_html__( 'Ubah Spanduk', 'w9-childwp' ),
            'search_items'       => esc_html__( 'Cari Spanduk', 'w9-childwp' ),
            'not_found'          => esc_html__( 'Tidak Ada', 'w9-childwp' ),
            'not_found_in_trash' => esc_html__( 'Kosong', 'w9-childwp' ),
        );

        $args = array(
            'label'               => esc_html__( 'wl_9_Spanduk', 'w9-childwp' ),
            'description'         => esc_html__( 'Spanduk', 'w9-childwp' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'excerpt', 'author', 'thumbnail', 'revisions', 'post-formats' ),
            'taxonomies'          => array( 'jenis' ),
            'hierarchical'        => false,
            'public'              => true,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 18,
            'menu_icon'           => 'dashicons-cover-image',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'rewrite'             => array( 'slug' => $wl_9_spanduk_slug ),
        );

        register_post_type( 'ws-spanduk', $args );

    }
    add_action( 'init', 'wl_9_spanduk', 0 );
}

if ( ! function_exists( 'w9_jenis' ) ) {

    function w9_jenis() {
    
        $labels = array(
            'name'                       => esc_html_x( 'Jenis', 'General', 'w9-childwp' ),
            'singular_name'              => esc_html_x( 'Jenis', 'Singular', 'w9-childwp' ),
            'menu_name'                  => esc_html__( 'Jenis', 'w9-childwp' ),
            'all_items'                  => esc_html__( 'All Jenis', 'w9-childwp' ),
            'parent_item'                => esc_html__( 'Parent Jenis', 'w9-childwp' ),
            'parent_item_colon'          => esc_html__( 'Parent Jenis:', 'w9-childwp' ),
            'new_item_name'              => esc_html__( 'Tambah Jenis', 'w9-childwp' ),
            'add_new_item'               => esc_html__( 'Tambah Baru', 'w9-childwp' ),
            'edit_item'                  => esc_html__( 'Edit Jenis', 'w9-childwp' ),
            'update_item'                => esc_html__( 'Ubah Jenis', 'w9-childwp' ),
            'separate_items_with_commas' => esc_html__( 'Pisah Jenis dengan koma', 'w9-childwp' ),
            'search_items'               => esc_html__( 'Cari Jenis', 'w9-childwp' ),
            'add_or_remove_items'        => esc_html__( 'Tambah/Hapus', 'w9-childwp' ),
            'choose_from_most_used'      => esc_html__( 'Pilih Jenis populer', 'w9-childwp' ),
            'not_found'                  => esc_html__( 'Tidak ada', 'w9-childwp' ),
        );
    
        $args = array(
            'labels'            => $labels,
            'hierarchical'      => true,
            'public'            => true,
            'show_ui'           => true,
            'show_in_rest'      => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_tagcloud'     => false,
        );
    
        register_taxonomy( 'ws-jenis', array( 'ws-spanduk' ), $args );
    
    }
    add_action( 'init', 'w9_jenis', 0 );
}